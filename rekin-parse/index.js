'use strict'

// restart from here for more complete implementation : https://github.com/rehypejs/rehype/blob/master/packages/rehype-parse/index.js

import gherkin       from 'gherkin'
import AstBuilder    from 'gherkin/dist/src/AstBuilder'
import Parser        from 'gherkin/dist/src/Parser'
import TokenScanner  from 'gherkin/dist/src/TokenScanner'
import TokenMatcher  from 'gherkin/dist/src/TokenMatcher'
import streamToArray from 'stream-to-array'

import fromGherkinAST from '../gast-util-from-gherkin/'


export default parse

function parse(options) {

  this.Parser = parser

  //this is a sync version of the parser
  function parser(doc, vFile) {
    
    let featureContent = vFile.toString()
    
    //from : https://github.com/cucumber/gherkin-javascript/blob/e1d78558221b34b68af3e601ad6dc1b67895137b/test/ParserTest.ts#L13
    const parser = new Parser(new AstBuilder())
    const scanner = new TokenScanner(featureContent)
    const matcher = new TokenMatcher()

    const gherkinAST = parser.parse(scanner, matcher)

    return fromGherkinAST(gherkinAST);

    function onerror(err) {
      console.log('TODO : error management')
    }
  }
}
