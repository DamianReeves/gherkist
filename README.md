* This is an exploration version of Gast : the Gherkin AST based on unist ecosystem. 

* This librairy is not still npm'ed.
  * So, for now you can clone the repo in you src folder.
* Open to issues and pull requests ! 

* How to use it : 
``` javascript
import vfile from 'to-vfile'
import toString from 'nlcst-to-string'
import inspect from 'unist-util-inspect'
import gherkist from './gherkist'
import unified from 'unified'

const testFile = 'gherkist/example/gherkin-1.feature'

unified()
  .use(gherkist)
  .use(astViewer)
  .use(gastCompiler)
  .process(vfile.readSync(`src/${testFile}`), logFile)

// Utilities functions 

function astViewer() {
  return transformer
  function transformer(tree) {
    console.log('*******')
    console.log('AST inspector :')
    console.log(inspect(tree))
    console.log('*******')
  }
}


function gastCompiler(){

  this.Compiler = compiler

  function compiler(gast, vFile){
    //return JSON.stringify(gast,null,4)
    //return 'Dummy string'
    return inspect(gast)
  }
}


function logFile(err, file) {
  if (err) throw err

  console.log('==========')
  console.log(file)
  console.log('==========')
}
```

* Licence : Apache 2